Source: libdebug
Section: devel
Priority: optional
Maintainer: Peter Pentchev <roam@ringlet.net>
DM-Upload-Allowed: yes
Build-Depends: debhelper (>> 8.1.3~), dpkg-dev (>= 1.15.7~), hardening-includes
Standards-Version: 3.9.2
Homepage: http://devel.ringlet.net/devel/libdebug/
Vcs-Git: git://gitorious.org/libdebug/pkg-debian.git
Vcs-Browser: http://gitorious.org/libdebug/pkg-debian

Package: libdebug0
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: libdebug0-dev (>= ${binary:Version})
Description: Memory leak detection system and logging library
 This library contains routines needed by many of the authors
 other programs. It includes utilities to aid in debugging of
 programs.
 .
 Some of the features currently supported by this library are
 memory leak detection, hex dumping, and logging. The logging
 system is especially useful. It adds syntax highlighting for
 different log levels in debugging mode. There are different
 modes for printing to console and files, duplicate line
 detection and lots of other small things which makes life
 simpler for the programmer.
 .
 This package contains files needed by programs linked against
 this library.

Package: libdebug0-dev
Architecture: any
Section: libdevel
Multi-Arch: foreign
Depends: ${misc:Depends}, libdebug0 (= ${binary:Version})
Description: Development files for the debug library
 This library contains routines needed by many of the authors
 other programs. It includes utilities to aid in debugging of
 programs.
 .
 Some of the features currently supported by this library are
 memory leak detection, hex dumping, and logging. The logging
 system is especially useful. It adds syntax highlighting for
 different log levels in debugging mode. There are different
 modes for printing to console and files, duplicate line
 detection and lots of other small things which makes life
 simpler for the programmer.
 .
 This package contains files needed if you wish to use libdebug
 in your own programs.
